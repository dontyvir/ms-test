const loggerBECH = require("./loggerBECH.service").loggerBECH;

/**
 * Middleware para manejo de excepciones dentro de controladores
 * @param {Error} err
 * @param {Http Request} req
 * @param {Http Response} res
 * @param {function} next
 * @returns {Promise<void>}
 */
async function errorHandlerService(err, req, res, next) {
  loggerBECH.error(err.message);

  if (!err.statusCode) err.statusCode = 500;

  res.status(err.statusCode).send({ error: err.message });
  next();
}

module.exports = errorHandlerService;
