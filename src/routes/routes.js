"use strict";

const hola = require("../controllers/hola.controller");

const { healthcheck } = require("../controllers/healthcheck.controller");
const errorHandler = require("../services/errorHandler.service");

function routes(app) {
  app.get("/hola", hola);

  // Operación de healthcheck
  app.get("/healthcheck", healthcheck);

  // Default route
  app.get("/*", (req, res) => {
    console.log(req.connection);
    return res.status(200).send("OK");
  });

  // Middleware para manejo de errores
  app.use(errorHandler);
}

module.exports = { routes };
